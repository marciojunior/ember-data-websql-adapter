var recordId, env, store, Post, Comment, User, all = Ember.RSVP.all, db;

function dateWithoutMilli(date) {
  return Math.round(date.getTime() / 1000);
}

function resultFromQueryEqual(query, expected) {
  return sql(db, query).then(async(function(result) {
    deepEqual(result, expected, query);
  }), Ember.RSVP.rethrow);
}

module('DS.WebSqlAdapter', {
  setup: function() {
    recordId = 0;

    Post = DS.Model.extend({
      isEditable: DS.attr('boolean'),
      title: DS.attr('string'),
      views: DS.attr('number'),
      createdTime: DS.attr('date')
    });

    Post.toString = function() {
      return "Post";
    };

    Comment = DS.Model.extend({
      content: DS.attr('string')
    });

    Comment.toString = function() {
      return "Comment";
    };

    User = DS.Model.extend({
      name: DS.attr('string')
    });

    User.toString = function() {
      return "User";
    };

    Post.reopen({
      comments: DS.hasMany('comment', { async: true })
    });

    Comment.reopen({
      user: DS.belongsTo('user'),
      post: DS.belongsTo('post')
    });

    User.reopen({
      comment: DS.hasOne('comment', { async: true })
    });

    env = setupStore({
      post: Post,
      comment: Comment,
      user: User,
      adapter: DS.WebSqlAdapter.extend({
        name: 'ember-data-websql-adapter-tests' ,
        generateIdForRecord: function() {
          return ++recordId;
        }
      })
    });
    store = env.store;
    store.modelFor('post');
    store.modelFor('comment');
    store.modelFor('user');
    db = connect('ember-data-websql-adapter-tests');

    stop();
    env.adapter.addEntity(Post);
    env.adapter.addEntity(Comment);
    env.adapter.addEntity(User);
    env.adapter.createTables().then(function() {
      start();
    });
  },
  teardown: function() {
    env.container.destroy();
    store.destroy();
    stop();
    dropTables(db).then(function() {
      start();
    });
  }
});

test('Schema generation', function() {
  all([tableExists(db, 'post'), tableExists(db, 'comment'), tableExists(db, 'user')]).then(async(function(results) {
    var post = results[0],
    comment = results[1],
    user = results[2];
    ok(post, 'post table created');
    ok(comment, 'comment table created');
    ok(user, 'user table created');
  }));
});

test('Insert record', function() {
  var now = new Date();
  var post = store.createRecord('post', { title: 'Functional reactive programming', views: 5, createdTime: now, isEditable: false });

  post.save().then(async(function(post) {
    equal(post.get('title'), 'Functional reactive programming');
    resultFromQueryEqual('SELECT * FROM post', [
      { id: "1", title: 'Functional reactive programming', views: 5, createdTime: dateWithoutMilli(now), isEditable: 0 }
    ]);
  }));
});

test('Update record', function() {
  var post = store.createRecord('post', { title: 'Functional reactive programming' });

  post.save().then(async(function(post) {
    post.set('title', 'Rails is omakase');
    post.set('isEditable', true);

    return post.save().then(async(function(post) {
      equal(post.get('title'), 'Rails is omakase');
      equal(post.get('isEditable'), true);
      resultFromQueryEqual('SELECT * FROM post', [
        { id: "1", title: 'Rails is omakase', isEditable: 1, views: null, createdTime: null }
      ]);
    }));

  }));
});

test('Delete record', function() {
  var post = store.createRecord('post', { title: 'Functional reactive programming' });

  post.save().then(async(function(post) {
    post.deleteRecord();

    return post.save().then(async(function(post) {
      return store.find('post').then(async(function(posts) {
        equal(posts.get('length'), 0);
        resultFromQueryEqual('SELECT * FROM post', []);
      }));
    }));

  }));
});

test('Find all records', function() {
  var now = new Date();
  var data = all([
    sql(db, 'INSERT INTO post (id, title, views, isEditable, createdTime) VALUES(?, ?, ?, ?, ?)', ['1', 'Functional reactive programming', 3, 1, +now]),
    sql(db, 'INSERT INTO post (id, title, views, isEditable, createdTime) VALUES(?, ?, ?, ?, ?)', ['2', 'Rails is omakase', 2, 0, +now])
  ]);

  data.then(async(function() {

    return store.find('post').then(async(function(posts) {
      equal(posts.get('length'), 2);
      deepEqual(posts.mapBy('title'), ['Functional reactive programming', 'Rails is omakase']);
      deepEqual(posts.mapBy('views'), [3, 2]);
      deepEqual(posts.mapBy('isEditable'), [true, false]);
      equal(dateWithoutMilli(posts.objectAt(0).get('createdTime')), dateWithoutMilli(now));
      equal(dateWithoutMilli(posts.objectAt(1).get('createdTime')), dateWithoutMilli(now));
    }));

  }));
});

test('Find records by id', function() {
  var now = new Date();
  var data = all([
    sql(db, 'INSERT INTO post (id, title, views, isEditable, createdTime) VALUES(?, ?, ?, ?, ?)', ['1', 'Functional reactive programming', 3, 1, +now]),
    sql(db, 'INSERT INTO post (id, title, views, isEditable, createdTime) VALUES(?, ?, ?, ?, ?)', ['2', 'Rails is omakase', 2, 0, +now])
  ]);

  data.then(async(function() {
    return store.find('post', '1').then(async(function(postFound) {
      ok(postFound);
      equal(postFound.get('title'), 'Functional reactive programming');
      equal(postFound.get('views'), 3);
      equal(postFound.get('isEditable'), true);
      equal(dateWithoutMilli(postFound.get('createdTime')), dateWithoutMilli(now));
    }));

  }));
});

test('Find records by query', function() {
  var data = all([
    sql(db, 'INSERT INTO post (id, title) VALUES(?, ?)', ['1', 'Functional reactive programming']),
    sql(db, 'INSERT INTO post (id, title) VALUES(?, ?)', ['2', 'Rails is omakase'])
  ]);

  data.then(async(function() {
    return store.find('post', { title: 'Rails is omakase' }).then(async(function(posts) {
      var post = posts.objectAt(0);
      equal(posts.get('length'), 1);
      equal(post.get('title'), 'Rails is omakase');

      return store.find('post', { title: 'Functional reactive programming' }).then(async(function(posts) {
        var post = posts.objectAt(0);
        equal(posts.get('length'), 1);
        equal(post.get('title'), 'Functional reactive programming');
      }));
    }));

  }));
});

test('Has many / Insert record with relationship', function() {
  var now = new Date();
  var post = store.createRecord('post', { title: 'Functional reactive programming' });
  var comment = store.createRecord('comment', { content: 'Awesome!', post: post });

  all([post.save(), comment.save()]).then(async(function(records) {
    var post = records[0],
    comment = records[1];
    equal(post.get('title'), 'Functional reactive programming');
    equal(comment.get('content'), 'Awesome!');
    resultFromQueryEqual('SELECT id, title FROM post', [
      { id: "1", title: 'Functional reactive programming' }
    ]);
    resultFromQueryEqual('SELECT id, content, post_id FROM comment', [
      { id: "2", content: 'Awesome!', post_id: "1" }
    ]);
  }));
});

test('Has many / Update record from relationship', function() {
  var post = store.createRecord('post', { title: 'Functional reactive programming' });
  var comment = store.createRecord('comment', { content: 'Awesome!' });
  post.get('comments').pushObject(comment);

  all([post.save(), comment.save()]).then(async(function(records) {
    var post = records[0],
    comment = records[1];
    post.set('title', 'Rails is omakase');
    post.set('isEditable', true);
    comment = store.createRecord('comment', { content: 'Wow!' });
    post.get('comments').pushObject(comment);

    return all([post.save(), comment.save()]).then(async(function(records) {
      var post = records[0],
      comment = records[1];
      equal(post.get('title'), 'Rails is omakase');
      equal(post.get('isEditable'), true);

      post.get('comments').then(async(function(comments) {
        equal(comments.get('length'), 2);
      }));

      resultFromQueryEqual('SELECT id, title, isEditable FROM post', [
        { id: "1", title: 'Rails is omakase', isEditable: 1 }
      ]);

      resultFromQueryEqual('SELECT id, content, post_id FROM comment', [
        { id: "2", content: 'Awesome!', post_id: "1" },
        { id: "3", content: 'Wow!', post_id: "1" }
      ]);
    }));

  }));
});

test('Has many / Delete record from relationship', function() {
  var post = store.createRecord('post', { title: 'Functional reactive programming' });
  var comment = store.createRecord('comment', { content: 'Awesome!' });
  post.get('comments').pushObject(comment);

  all([post.save(), comment.save()]).then(async(function(records) {
    var post = records[0],
    comment = records[1];
    comment.deleteRecord();

    return all([post.save(), comment.save()]).then(async(function(records) {
      var post = records[0],
      comment = records[1];

      return all([store.find('post'), store.find('comment')]).then(async(function(records) {
        var posts = records[0],
        comments = records[1];

        equal(posts.get('length'), 1);
        equal(comments.get('length'), 0);

        resultFromQueryEqual('SELECT id, title FROM post', [{ id: '1', title: 'Functional reactive programming' }]);
        resultFromQueryEqual('SELECT * FROM comment', []);
      }));
    }));

  }));
});

test('Has many / Find all records from relationship', function() {
  var data = all([
    sql(db, 'INSERT INTO post (id, title) VALUES(?, ?)', ['1', 'Functional reactive programming']),
    sql(db, 'INSERT INTO comment (id, content, post_id) VALUES(?, ?, ?)', ['1', 'Whoa!', "1"])
  ]);

  data.then(async(function() {

    return store.find('post').then(async(function(posts) {
      equal(posts.get('length'), 1);
      var post = posts.objectAt(0);

      equal(post.get('title'), 'Functional reactive programming');

      post.get('comments').then(async(function(comments) {
        equal(comments.get('length'), 1);
        equal(comments.get('firstObject.content'), 'Whoa!');
      }));
    }));

  }));
});

test('Has many / Find all records from relationship when fetching one model', function() {
  var data = all([
    sql(db, 'INSERT INTO post (id, title) VALUES(?, ?)', ['1', 'Functional reactive programming']),
    sql(db, 'INSERT INTO comment (id, content, post_id) VALUES(?, ?, ?)', ['1', 'Whoa!', "1"])
  ]);

  data.then(async(function() {

    return store.find('post' , 1).then(async(function(post) {
      equal(post.get('title'), 'Functional reactive programming');

      post.get('comments').then(async(function(comments) {
        equal(comments.get('length'), 1);
        equal(comments.get('firstObject.content'), 'Whoa!');
      }));
    }));

  }));
});

test('Has one / Insert record with relationship', function() {
  var now = new Date();
  var user = store.createRecord('user', { name: 'Marcio' });
  var comment = store.createRecord('comment', { content: 'Awesome!', user: user });

  all([user.save(), comment.save()]).then(async(function(records) {
    var user = records[0],
    comment = records[1];
    equal(user.get('name'), 'Marcio');
    equal(comment.get('content'), 'Awesome!');

    resultFromQueryEqual('SELECT id, name FROM user', [
      { id: "1", name: 'Marcio' }
    ]);

    resultFromQueryEqual('SELECT id, content, user_id FROM comment', [
      { id: "2", content: 'Awesome!', user_id: "1" }
    ]);
  }));
});

test('Has one / Update record from relationship', function() {
  var user = store.createRecord('user', { name: 'Marcio' });
  var comment = store.createRecord('comment', { content: 'Awesome!', user: user });

  all([user.save(), comment.save()]).then(async(function(records) {
    var user = records[0],
    comment = records[1];
    user.set('name', 'Marcio Junior');
    comment = store.createRecord('comment', { content: 'Wow!', user: user });

    return all([user.save(), comment.save()]).then(async(function(records) {
      var user = records[0],
      comment = records[1];

      equal(user.get('name'), 'Marcio Junior');
      equal(user.get('comment.content'), comment);

      resultFromQueryEqual('SELECT id, name FROM user', [
        { id: "1", name: 'Marcio Junior' }
      ]);

      resultFromQueryEqual('SELECT id, content, user_id FROM comment', [
        { id: "2", content: 'Awesome!', user_id: "1" }, // TODO user_id needs to be null
        { id: "3", content: 'Wow!', user_id: "1" }
      ]);
    }));

  }));
});

test('Has one / Delete record from relationship', function() {
  var user = store.createRecord('user', { name: 'Marcio' });
  var comment = store.createRecord('comment', { content: 'Awesome!' });
  user.set('comment', comment);

  all([user.save(), comment.save()]).then(async(function(records) {
    var user = records[0],
    comment = records[1];
    comment.deleteRecord();

    return all([user.save(), comment.save()]).then(async(function(records) {
      var user = records[0],
      comment = records[1];

      return all([store.find('user'), store.find('comment')]).then(async(function(records) {
        var posts = records[0],
        comments = records[1];

        equal(posts.get('length'), 1);
        equal(comments.get('length'), 0);
        // equal(user.get('comment'), null); TODO this must be null

        resultFromQueryEqual('SELECT id, name FROM user', [{ id: '1', name: 'Marcio' }]);
        resultFromQueryEqual('SELECT * FROM comment', []);
      }));
    }));

  }));
});

test('Has one / Find the record from relationship', function() {
  var data = all([
    sql(db, 'INSERT INTO user (id, name) VALUES(?, ?)', ['1', 'Marcio']),
    sql(db, 'INSERT INTO comment (id, content, user_id) VALUES(?, ?, ?)', ['1', 'Whoa!', "1"])
  ]);

  data.then(async(function() {

    return store.find('user').then(async(function(users) {
      equal(users.get('length'), 1);
      var user = users.objectAt(0);

      equal(user.get('name'), 'Marcio');

      user.get('comment').then(async(function(comment) {
        ok(comment);
        equal(comment.get('content'), 'Whoa!');
      }));
    }));

  }));
});