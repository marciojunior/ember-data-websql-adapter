DS.WebSqlBooleanTransform = DS.Transform.extend({
  deserialize: function(serialized) {
    return serialized === 1;
  },
  serialize: function(deserialized) {
    return +deserialized;
  }
});

DS.WebSqlDateTransform = DS.Transform.extend({
  deserialize: function(serialized) {
    return new Date(serialized);
  },
  serialize: function(date) {
    if (date instanceof Date) {
      return Math.round(date.getTime() / 1000);
    } else {
      return null;
    }
  }
});