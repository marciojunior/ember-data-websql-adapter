var get = Ember.get, set = Ember.set;

Ember.onLoad('Ember.Application', function(Application) {
  Application.initializer({
    name: "register_entities",
    initialize: function(container, application) {
      var adapter = container.lookup('adapter:application');
      var store = container.lookup('store:main');
      if (!DS.WebSqlAdapter.detectInstance(adapter)) { return; }
      adapter.setEntities(entities);

      entities.forEach(function(entity) {
        store.modelFor(entity);
      });

      application.deferReadiness();
      adapter.createTables().then(function() {
        application.advanceReadiness();
      });
    }
  });
});

var entities = [];
var oldExtend = DS.Model.extend;

DS.Model.extend = function() {
  var Clazz = oldExtend.apply(this, arguments);
  entities.push(Clazz);
  return Clazz;
};

DS.WebSqlAdapter = DS.Adapter.extend({
  defaultSerializer: '_webSql',
  schemaGenerator: new DS.SchemaGenerator(),
  init: function() {
    this._super();
    var name = get(this, 'name'),
    version = get(this, 'version') || '1',
    description = get(this, 'description') || '',
    size = get(this, 'size') || 2 * 1024 * 1024;

    Ember.assert('Schema name must be present', name);

    this.database = openDatabase(name, version, description, size);
  },
  addEntity: function(type) {
    this.schemaGenerator.addEntity(type);
  },
  setEntities: function(entities) {
    this.schemaGenerator.entities = entities;
  },
  tableNameFor: function(type) {
    return this.schemaGenerator.tableNameFor(type);
  },
  foreignKeyNameFor: function(type) {
    return this.schemaGenerator.foreignKeyNameFor(type);
  },
  createTables: function() {
    var sqls = this.schemaGenerator.generateSqls();
    var promises = [];
    for(var i = 0; i < sqls.length; i++) {
      promises.push(this.sql(sqls[i]));
    }
    return Ember.RSVP.all(promises);
  },
  generateIdForRecord: function() {
    var s = [];
    var hexDigits = '0123456789ABCDEF';
    for ( var i = 0; i < 32; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[12] = '4';
    s[16] = hexDigits.substr((s[16] & 0x3) | 0x8, 1);

    var uuid = s.join('');
    return uuid;
  },
  nativeTransaction: function(fn) {
    this.database.transaction(function(tx) {
      fn(tx);
    });
  },
  sql: function(sql, values) {
    var adapter = this;
    return new Ember.RSVP.Promise(function(resolve, reject) {
      adapter.nativeTransaction(function(tx) {
        tx.executeSql(sql, values, function(tx, rs) {
          var rows = rs.rows;
          var result = [];
          for(var i = 0; i < rows.length; i++) {
            // results from rows.item(n) seems immutable, we copy to be able to change in serializer.extract
            result.push(Ember.copy(rows.item(i)));
          }
          Ember.run(null, resolve, result);
        }, function(tx, error) {
          Ember.run(null, reject, error);
          Ember.RSVP.rethrow(error);
          return true;
        });
      });
    });
  },
  find: function(store, type, id) {
    var tableName = this.tableNameFor(type);
    return this.sql('SELECT * FROM ' + tableName + ' WHERE id = ?', [id]).then(function(result) {
      return result.length > 0 ? result[0] : null;
    });
  },
  translateQuery: function(store, type, query) {
    var tableName = this.tableNameFor(type),
    serializer = store.serializerFor(type),
    serializedQuery = {},
    queryParts = [],
    params = [];

    type.eachAttribute(function(key, attribute) {
      if (key in query) {
        serializer.serializeAttribute(query, serializedQuery, key, attribute);
        queryParts.push(key + ' = ? ');
        params.push(serializedQuery[key]);
      }
    }, this);

    var sqlQuery = 'SELECT * FROM ' + tableName + ' WHERE ' + queryParts.join('AND');

    return { query: sqlQuery, params: params };
  },
  findQuery: function(store, type, query) {
    var tuple = this.translateQuery(store, type, query);
    return this.sql(tuple.query, tuple.params);
  },
  findHasMany: function(store, record, relationship) {
    var tableName = this.tableNameFor(relationship.type),
    foreignKeyName = this.foreignKeyNameFor(record.constructor),
    id = get(record, 'id');

    return this.sql('SELECT * FROM ' + tableName + ' WHERE ' + foreignKeyName + ' = ?', [id]);
  },
  findBelongsTo: function(store, record, relationship) {
    var tableName = this.tableNameFor(relationship.type),
    foreignKeyName = this.foreignKeyNameFor(record.constructor),
    id = get(record, 'id');

    return this.sql('SELECT * FROM ' + tableName + ' WHERE ' + foreignKeyName + ' = ?', [id]).then(function(result) {
      return result.length > 0 ? result[0] : null;
    });
  },
  findAll: function(store, type) {
    var tableName = this.tableNameFor(type);
    return this.sql('SELECT * FROM ' + tableName);
  },
  createRecord: function(store, type, record) {
    var tableName = this.tableNameFor(type),
    serializer = store.serializerFor(type),
    data = serializer.serialize(record, { includeId: true }),
    columns = [],
    values = [],
    questionMarks = [];

    for(var key in data) {
      columns.push(key);
      values.push(data[key]);
      questionMarks.push('?');
    }

    var query = 'INSERT INTO ' + tableName + ' (' + columns + ') VALUES (' + questionMarks + ')';
    return this.sql(query, values);
  },
  updateRecord: function(store, type, record) {
    var tableName = this.tableNameFor(type),
    serializer = store.serializerFor(type),
    data = serializer.serialize(record),
    columns = [],
    values = [],
    id = get(record, 'id');

    for(var key in data) {
      values.push(data[key]);
      columns.push(key + ' = ?');
    }
    values.push(id);

    var query = 'UPDATE ' + tableName + ' SET ' + columns.join(', ') + ' WHERE id = ?';
    return this.sql(query, values);
  },
  deleteRecord: function(store, type, record) {
    var tableName = this.tableNameFor(type),
    id = get(record, 'id');

    var query = 'DELETE FROM ' + tableName + ' WHERE id = ?';
    return this.sql(query, [id]);
  }
});