DS.hasOne = DS.belongsTo;

var oldBelongsTo = DS.belongsTo;

DS.belongsTo = function(type, options) {
  options = options || {};
  options.belongsTo = true;
  return oldBelongsTo(type, options);
};