var get = Ember.get;

Ember.onLoad('Ember.Application', function(Application) {
  Application.initializer({
    name: "inject_websql_serializer",
    initialize: function(container, application) {
      application.register('serializer:_webSql', DS.WebSqlSerializer);
    }
  });
});

function emptyFunc(arguments) {
}

DS.WebSqlSerializer = DS.JSONSerializer.extend({
  extractCreateRecord: emptyFunc,
  extractUpdateRecord: emptyFunc,
  extractDeleteRecord: emptyFunc,
  injectLinks: function(type, payload) {
    type.eachRelationship(function(key, relationship) {
      for(var prop in payload) {
        if (!payload.hasOwnProperty(prop)) continue;
        var item = payload[prop];
        if (relationship.kind === 'belongsTo' && !relationship.options.belongsTo) {
          item.links = {};
          item.links[relationship.key] = relationship;
        } else if (relationship.kind === 'hasMany') {
          item.links = {};
          item.links[relationship.key] = relationship;
        }
      }
    });
  },
  extractSingle: function(store, type, payload) {
    this.injectLinks(type, [payload]);
    return this.normalize(type, payload);
  },
  extractArray: function(store, type, payload) {
    var self = this;
    return payload.map(function(data) {
      self.injectLinks(type, payload);
      return self.normalize(type, data);
    });
  },
  serialize: function(record, options) {
    var json = {};

    if (options && options.includeId) {
      var id = get(record, 'id');

      if (id) {
        json[get(this, 'primaryKey')] = get(record, 'id');
      }
    }

    record.eachAttribute(function(key, attribute) {
      this.serializeAttribute(record, json, key, attribute);
    }, this);

    record.eachRelationship(function(key, relationship) {
      if (relationship.kind === 'belongsTo' && relationship.options.belongsTo) {
        this.serializeBelongsTo(record, json, relationship);
      } else if (relationship.kind === 'hasMany') {
        this.serializeHasMany(record, json, relationship);
      }
    }, this);

    return json;
  },
  serializeBelongsTo: function(record, json, relationship) {
    var key = relationship.key;

    var belongsTo = get(record, key);

    if (Ember.isNone(belongsTo)) { return; }

    json[key + '_id'] = get(belongsTo, 'id');

    // if (relationship.options.polymorphic) {
    //   json[key + "_type"] = belongsTo.constructor.typeKey;
    // }
  },

});