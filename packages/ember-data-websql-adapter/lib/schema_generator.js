function SchemaGenerator() {
  this.entities = [];
}

SchemaGenerator.prototype = {
  addEntity: function(type) {
    this.entities.push(type);
  },
  generateSqls: function() {
    var sql = [];
    for(var i = 0; i < this.entities.length; i++) {
      var entity = this.entities[i];
      sql.push(this._buildCreateTableSql(entity));
    }
    return sql;
  },
  resolveWebSqlType: function(type) {

    var data = {
      'number': 'REAL',
      'string': 'TEXT',
      'boolean': 'INTEGER',
      'date': 'INTEGER'
    };

    return data[type];
  },
  foreignKeyNameFor: function(type) {
    return this.tableNameFor(type) + '_id';
  },
  tableNameFor: function(type) {
    var typeName = type.toString();
    if (typeName.indexOf('.') !== -1) {
      var index = typeName.lastIndexOf('.');
      typeName = typeName.substring(index + 1, typeName.length);
    }
    return Ember.String.underscore(typeName.toString());
  },
  _buildCreateTableSql: function(type) {
    var columnNameByDbType = [];
    columnNameByDbType.push('id TEXT');

    type.eachAttribute(function(name, attr) {
      var webSqlType = this.resolveWebSqlType(attr.type);
      columnNameByDbType.push(name + ' ' + webSqlType);
    }, this);

    type.eachRelationship(function(key, relationship) {
      if (relationship.kind === 'belongsTo' && relationship.options.belongsTo) {
        var foreignKeyName = this.foreignKeyNameFor(relationship.type);
        columnNameByDbType.push(foreignKeyName + ' TEXT');
      }
    }, this);

    var sql = 'CREATE TABLE IF NOT EXISTS ' + this.tableNameFor(type) + '(' + columnNameByDbType.join(', ') + ')';
    return sql;
  }
};

window.DS.SchemaGenerator = SchemaGenerator;