require("ember-data-websql-adapter/attributes");
require("ember-data-websql-adapter/schema_generator");
require("ember-data-websql-adapter/serializer");
require("ember-data-websql-adapter/adapter");
require("ember-data-websql-adapter/transforms");