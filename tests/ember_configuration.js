/*globals EmberDev ENV QUnit */

(function() {
  window.Ember = window.Ember || {};

  Ember.config = {};
  Ember.testing = true;

  window.ENV = { TESTING: true };

  var extendPrototypes = QUnit.urlParams.extendprototypes;
  ENV['EXTEND_PROTOTYPES'] = !!extendPrototypes;

  if (EmberDev.jsHint) {
    // jsHint makes its own Object.create stub, we don't want to use this
    ENV['STUB_OBJECT_CREATE'] = !Object.create;
  }

  window.async = function(callback, timeout) {
    stop();

    timeout = setTimeout(function() {
      start();
      ok(false, "Timeout was reached");
    }, timeout || 100);

    return function() {
      clearTimeout(timeout);

      start();

      var args = arguments;
      Ember.run(function() {
        callback.apply(this, args);
      });
    };
  };

  window.invokeAsync = function(callback, timeout) {
    timeout = timeout || 1;

    setTimeout(async(callback, timeout+100), timeout);
  };

  window.setupStore = function(options) {
    var env = {};
    options = options || {};

    var container = env.container = new Ember.Container();

    var adapter = env.adapter = (options.adapter || DS.Adapter);
    delete options.adapter;

    for (var prop in options) {
      container.register('model:' + prop, options[prop]);
    }

    container.register('store:main', DS.Store.extend({
      adapter: adapter
    }));

    container.register('serializer:_webSql', DS.WebSqlSerializer);
    container.register('transform:boolean', DS.WebSqlBooleanTransform);
    container.register('transform:date', DS.WebSqlDateTransform);
    container.register('transform:number', DS.NumberTransform);
    container.register('transform:string', DS.StringTransform);

    container.injection('serializer', 'store', 'store:main');

    env.serializer = container.lookup('serializer:_webSql');
    env.store = container.lookup('store:main');
    env.adapter = env.store.get('defaultAdapter');

    return env;
  };

  var syncForTest = function(fn) {
    var callSuper;

    if (typeof fn !== "function") { callSuper = true; }

    return function() {
      var override = false, ret;

      if (Ember.run && !Ember.run.currentRunLoop) {
        Ember.run.begin();
        override = true;
      }

      try {
        if (callSuper) {
          ret = this._super.apply(this, arguments);
        } else {
          ret = fn.apply(this, arguments);
        }
      } finally {
        if (override) {
          Ember.run.end();
        }
      }

      return ret;
    };
  };

  window.connect = function(options) {
    var name, version, description, size;
    if (typeof options === 'string') {
      name = options;
    } else {
      name = options.name;
      description = options.description;
      size = options.size;
      version = options.version;
    }

    description = description || '';
    size = size || 2 * 1024 * 1024;
    version = version || '1';

    Ember.assert('Schema name must be present', name);

    return openDatabase(name, version, description, size)
  }

  window.sql = function(database, sql, values) {
    return new Ember.RSVP.Promise(function(resolve, reject) {
      database.transaction(function(tx) {
        tx.executeSql(sql, values || [], function(tx, rs) {
          var rows = rs.rows;
          var result = [];
          for(var i = 0; i < rows.length; i++) {
            result.push(Ember.copy(rows.item(i)));
          }
          Ember.run(null, resolve, result);
        }, function(tx, error) {
          Ember.run(null, reject, error);
          ok(error.message, false);
          return true;
        });
      });
    });
  }

  window.tableExists = function(database, tableName) {
    return window.sql(database, "SELECT name FROM sqlite_master WHERE type='table' AND name=?", [tableName]).then(function(result) {
      return result.length > 0;
    });
  }

  window.dropTables = function(database) {
    return window.sql(database, "SELECT name FROM sqlite_master WHERE type='table'", []).then(function(tables) {
      var promises = [];
      tables.forEach(function(table) {
        if (table.name != '__WebKitDatabaseInfoTable__') {
          var promise = window.sql(database, 'DROP TABLE ' + table.name, []);
          promises.push(promise);
        }
      });
      return Ember.RSVP.all(promises);
    });
  }

  Ember.config.overrideAccessors = function() {
    Ember.set = syncForTest(Ember.set);
    Ember.get = syncForTest(Ember.get);
  };

  Ember.config.overrideClassMixin = function(ClassMixin) {
    ClassMixin.reopen({
      create: syncForTest()
    });
  };

  Ember.config.overridePrototypeMixin = function(PrototypeMixin) {
    PrototypeMixin.reopen({
      destroy: syncForTest()
    });
  };

  minispade.register('ember-data-websql-adapter/~test-setup', function() {
    Ember.RSVP.configure('onerror', function(reason) {
      // only print error messages if they're exceptions;
      // otherwise, let a future turn of the event loop
      // handle the error.
      if (reason && reason instanceof Error) {
        Ember.Logger.log(reason, reason.stack)
        throw reason;
      }
    });

    Ember.RSVP.resolve = syncForTest(Ember.RSVP.resolve);

    Ember.View.reopen({
      _insertElementLater: syncForTest()
    });

    DS.Store.reopen({
      save: syncForTest(),
      createRecord: syncForTest(),
      deleteRecord: syncForTest(),
      push: syncForTest(),
      pushMany: syncForTest(),
      filter: syncForTest(),
      find: syncForTest(),
      findMany: syncForTest(),
      findByIds: syncForTest(),
      didSaveRecord: syncForTest(),
      didSaveRecords: syncForTest(),
      didUpdateAttribute: syncForTest(),
      didUpdateAttributes: syncForTest(),
      didUpdateRelationship: syncForTest(),
      didUpdateRelationships: syncForTest()
    });

    DS.Model.reopen({
      save: syncForTest(),
      reload: syncForTest(),
      deleteRecord: syncForTest(),
      dataDidChange: Ember.observer(syncForTest(), 'data'),
      updateRecordArraysLater: syncForTest()
    });

    Ember.RSVP.Promise.prototype.then = syncForTest(Ember.RSVP.Promise.prototype.then);
  });

  EmberDev.distros = {
    spade:   'ember-spade.js',
    build:   'ember-data-websql-adapter.js'
  };
})();